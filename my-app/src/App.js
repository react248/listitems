import List from "./components/list";
const colorList = ["Orange", "Green", "Black", "Yellow"];
function App() {
  return <List listItems={colorList} />;
}

export default App;
